# Base Image: https://gitlab.cern.ch/drupal/paas/drupal-builder-base/
FROM gitlab-registry.cern.ch/drupal/paas/drupal-builder-base:dev

LABEL io.openshift.s2i.scripts-url="image:///usr/libexec/s2i" \
      io.s2i.scripts-url="image:///usr/libexec/s2i" \
      io.k8s.description="Drupal Builder Core" \
	io.openshift.expose-services="8080,9000:http" \
      io.k8s.display-name="Drupal Builder Core" \
      io.openshift.tags="builder,drupal" \
      maintainer="Drupal Admins <drupal-admins@cern.ch>"

COPY ./s2i/bin/ ./hooks/ /usr/libexec/s2i/
RUN chmod -R +x /usr/libexec/s2i/

# DRUPAL
# Path configuration
# Set up drupal site folder
RUN mkdir -p /var/www/html/

# Prepare folder to create necessary folders from composer
COPY ./drupal-spec/ /var/www/html/

WORKDIR /var/www/html/
RUN chmod -R a+rwx /var/www/html/

# Install composer. Enable cache for installing composer packages.
ENV COMPOSER_VERSION=1.8.4 COMPOSER_HOME=/var/www/html COMPOSER_CACHE_DIR=/var/www/html/.composer/
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer --version=${COMPOSER_VERSION}

# Do not run Composer as root/super user! See https://getcomposer.org/root for details
# Set up drupal minimum stack
RUN composer install --optimize-autoloader -vvv

# Clean-up composer installation
# Rename composer.json to composer.admins.json so that when user injects its composer,
# there is no conflict between then.
RUN mv /var/www/html/composer.json /var/www/html/composer.admins.json

# Add extra configurations
# - Simplesamlphp
COPY ./configuration/simplesamlphp/ /var/www/html/vendor/simplesamlphp/simplesamlphp/

# In order to drop the root user, we have to make some directories world
# writeable as OpenShift default security model is to run the container under
# random UID.
RUN chown -R 1001:0 /var/www/html/ \
 && chmod -R ug+rwx /var/www/html

# Include drupal tools in the PATH
ENV PATH=/var/www/html/vendor/bin:$PATH HOME=/var/www/html

USER 1001

CMD ["/usr/libexec/s2i/usage"]

# Private files
VOLUME /private/files
# Public files
VOLUME /var/www/html/web/sites/default/files